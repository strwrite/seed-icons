# Icons Collections for Seed

[Seed](https://seed-rs.org/) is an awesome framework to develop
single page applications.

This package provides `build.rs` to generate icons to use with Seed.
It does not provide you with icons themselves (you stil have to reference
CSS / JS files from your index.html), but it generates you a list of 
`.rs` files - one for each icon, which should give you a convenient way to 
use them in your application.

## Testing

To run icons rendering test you have two options:

### Run all tests on the firefox

``` bash
cargo make ff-test --no-workspace
```

This approach runs all tests, but it could fail to run inside Docker.

### Run only part of the tests on node

``` bash
cargo make in-node-test --no-workspace
```

That's the same test, as the one, that runs in CI.

### Run tests inside docker build

``` bash
cargo make ind-test --no-workspace
```

This test is even more close to CI run, 
but requires `docker` CLI to be installed and would not 
use local rust caches from `./target` directory.

Under the hood it runs `docker build` command with 
Dockerfile, which uses fixed `rust` version.