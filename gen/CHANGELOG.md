# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.1] - 2020-10-18
### Fixed
- removed gen_ files producing in source folder

## [0.3.0] - 2020-10-18
### Added
- material icons
- material icons download (only regular)
- registry functions for font-awesome and material-icons
- changelog
- Makefile.toml, using `cargo make` now
- env variable `SEED_ICONS_FILTER_NAMES` support

### Changed
- newer version of font-awesome: 5.14.0
- optimized font-awesome generation - now can use json from crate instead of dowloading