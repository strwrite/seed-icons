// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use crate::IconSource;
use crate::{errors::GenIconsErr, IconsCollectionSource};
use codegen::Scope;
use serde::Deserialize;
use std::fs::File;
use std::io::prelude::*;

pub static MI_RELEASE: &str = "3.0.1";

#[derive(Debug, Deserialize)]
struct MiIcon {
    name: String,
    tags: Vec<String>,
    categories: Vec<String>,
}

#[derive(Debug, Deserialize)]
struct MiResponse {
    icons: Vec<MiIcon>,
}

#[derive(Debug, Deserialize)]
pub struct MiIconSource {
    pub name: String,
    pub search_terms: Vec<String>,
}

impl crate::IconSource for MiIconSource {
    fn closure_view_with_classes_call(&self) -> String {
        format!(
            "crate::mi::default::{}::i_c(classes)",
            crate::module_name(&self.name)
        )
    }

    fn closure_view_call(&self) -> String {
        format!(
            "crate::mi::default::{}::i()",
            crate::module_name(&self.name)
        )
    }

    fn view_call_example(&self) -> String {
        format!(
            "seed_icons::mi::default::{}::i()",
            crate::module_name(&self.name)
        )
    }

    fn name(&self) -> String {
        self.name.clone()
    }
}

fn get_icons() -> Result<Vec<MiIconSource>, GenIconsErr> {
    let mut file = File::open("material-icons.json")?;

    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let resp: MiResponse = serde_json::from_str(&*contents)?;
    let res: Vec<MiIconSource> = resp
        .icons
        .into_iter()
        .map(|icon| {
            let mut search_terms: Vec<String> = vec![];
            search_terms.extend(icon.tags.iter().cloned());
            search_terms.extend(icon.categories.iter().cloned());
            MiIconSource {
                name: icon.name,
                search_terms: search_terms,
            }
        })
        .collect();
    Ok(res)
}

pub fn write_material_icons(
    scope: &mut Scope,
    enable_styles: bool,
    maybe_filter: Option<Vec<String>>,
) -> Result<(), GenIconsErr> {
    let module = scope.new_module("mi").vis("pub");

    let icons = get_icons()?;

    icons.into_iter().for_each(|icon| {
        let family_module_names = vec!["default", "outlined", "round", "sharp", "two_tone"];
        family_module_names
            .into_iter()
            .for_each(|family_module_name| {
                if maybe_filter
                    .clone()
                    .map(|filter| !filter.contains(&icon.name()))
                    .or_else(|| Some(false))
                    .unwrap()
                {
                    return;
                }

                let family_module = module.get_module_mut(family_module_name);
                let family_module: &mut codegen::Module = match family_module {
                    None => module.new_module(family_module_name).vis("pub"),
                    Some(family_module) => family_module,
                };

                let family_css_classes = format!(
                    r#""material-icons{}""#,
                    match family_module_name {
                        "default" => "".to_string(),
                        "two_tone" => "-two-tone".to_string(),
                        it => format!("-{}", it),
                    }
                );
                let module_name = crate::module_name(&icon.name);
                let module = family_module.new_module(&module_name).vis("pub");
                crate::add_imports(module, enable_styles);
                crate::add_functions(module, family_css_classes, enable_styles, Some(&icon.name))
            });
    });
    Ok(())
}

pub fn registry_icons(
    collections: &mut Vec<IconsCollectionSource>,
    maybe_filter: &Option<Vec<String>>,
) -> Result<(), GenIconsErr> {
    let mi_icons = get_icons()?;
    let mi_icons: Vec<Box<dyn IconSource>> = mi_icons
        .into_iter()
        .map(|it| {
            let boxed: Box<dyn IconSource> = Box::new(it);
            boxed
        })
        .filter(|icon| {
            maybe_filter
                .clone()
                .map(|filter| filter.contains(&icon.name()))
                .or_else(|| Some(true))
                .unwrap()
        })
        .collect();
    let mi_icons = IconsCollectionSource {
        icons: mi_icons,
        function_prefix: "material_icons".to_string(),
    };
    collections.push(mi_icons);
    Ok(())
}
