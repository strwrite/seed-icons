// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

use std::io::prelude::*;
use std::{fs::File, slice::Iter};

use crate::errors::GenIconsErr;
use codegen::Scope;

fn add_collection_bulk(
    scope: &mut Scope,
    iter: &mut Iter<Box<dyn crate::IconSource>>,
    num: i32,
    function_prefix: &String,
) {
    let name: &str = &format!("{}_collection_bulk{}", function_prefix, num);
    let collection_bulk = scope
        .new_fn(name)
        .generic("T")
        .vis("pub")
        .ret("Vec<RegisteredIcon<'static, T>>");
    collection_bulk.line("vec![");
    iter.by_ref().take(1000).for_each(|icon| {
        let view = icon.closure_view_call();
        collection_bulk.line(format!(
            r#"RegisteredIcon {{ 
                name: "{}".to_string(), 
                view: &move || {} ,
                view_with_classes: &move |classes| {}
            }},"#,
            icon.name(),
            view,
            icon.closure_view_with_classes_call(),
        ));
    });
    collection_bulk.line("]");
}

fn add_collection(
    scope: &mut Scope,
    icons: &Vec<Box<dyn crate::IconSource>>,
    function_prefix: &String,
) {
    // have to split into bulks to avoid 'locals exceed maximum' problem
    let mut iter = icons.into_iter();
    add_collection_bulk(scope, &mut iter, 1, function_prefix);
    add_collection_bulk(scope, &mut iter, 2, function_prefix);

    let collection_function = scope
        .new_fn(&*format!("{}_collection", function_prefix))
        .generic("T")
        .vis("pub")
        .ret("Vec<RegisteredIcon<'static, T>>");

    collection_function.line(format!(
        "let mut f1 = {}_collection_bulk1();",
        function_prefix
    ));
    collection_function.line(format!(
        "let mut f2 = {}_collection_bulk2();",
        function_prefix
    ));
    collection_function.line("f1.append(&mut f2);");
    collection_function.line("f1");
}

pub fn write_registry(
    collections: Vec<crate::IconsCollectionSource>,
    out_dir: &str,
) -> Result<(), GenIconsErr> {

    let mut scope = Scope::new();
    collections
        .into_iter()
        .for_each(|collection| {
            add_collection(&mut scope, &collection.icons,  &collection.function_prefix);
        });

    // write_to_file(&scope, "./gen_registry.rs".to_string())?;
    write_to_file(
        &scope,
        format!("{}/gen_registry.rs", out_dir),
    )?;
    Ok(())
}

fn write_to_file(scope: &Scope, filename: String) -> std::io::Result<()> {
    let mut file = File::create(filename)?;
    file.write_all(scope.to_string().as_bytes())?;
    Ok(())
}
