// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

extern crate tempfile;
extern crate zip;
use std::fs;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::Write;
use std::path::Path;

#[derive(Debug)]
pub enum GetResourcesError {
    IOError(io::Error),
    HttpError(attohttpc::Error),
    ZipError(zip::result::ZipError),
}

impl std::convert::From<io::Error> for GetResourcesError {
    fn from(error: io::Error) -> Self {
        GetResourcesError::IOError(error)
    }
}

impl std::convert::From<attohttpc::Error> for GetResourcesError {
    fn from(error: attohttpc::Error) -> Self {
        GetResourcesError::HttpError(error)
    }
}

impl std::convert::From<zip::result::ZipError> for GetResourcesError {
    fn from(error: zip::result::ZipError) -> Self {
        GetResourcesError::ZipError(error)
    }
}

pub fn download_fa(release: &str, target_dir: &str) -> Result<(), GetResourcesError> {
    let url = format!(
        "https://use.fontawesome.com/releases/v{}/fontawesome-free-{}-web.zip",
        release, release
    );
    download_and_unzip( "font-awesome", url, target_dir, None)
}

fn download_and_unzip(name: &str, url: String, target_dir: &str, maybe_prefix_filter: Option<&str>) -> Result<(), GetResourcesError>  {
    let root_target_dir_path = Path::new(target_dir);
    let target_icons_dir = root_target_dir_path.join(name);
    if target_icons_dir.exists() {
        return Ok(())
    }
    let mut tmpfile = tempfile::tempfile()?;
    let resp = attohttpc::get(url).send()?;
    let body = resp.bytes()?;
    tmpfile.write_all(&body)?;
    let mut zip = zip::ZipArchive::new(tmpfile)?;
    for i in 0..zip.len() {
        let mut file = zip.by_index(i)?;
        if file.is_file() {
            let source_name = file.name();
            let path = Path::new(source_name);
            let mut components = path.components();
            components.next();
            let cmp_path = components.as_path();
            let path = Path::new(name).join(cmp_path);
            match maybe_prefix_filter {
                Some(filter) if !cmp_path.starts_with(filter) => continue,
                _ => ()
            }
            let maybe_parent = path.parent();

            let res: Option<io::Result<()>> = maybe_parent.and_then(|dir| {
                let target_file_dir_path = root_target_dir_path.join(dir);
                let maybe_target_file_dir = target_file_dir_path.to_str();
                let res: Option<io::Result<()>> = maybe_target_file_dir
                    .map(|target_file_dir| fs::create_dir_all(target_file_dir));
                res
            });
            match res {
                // TODO: come up with a better syntax
                Some(Err(e)) => Err(e)?,
                _ => (),
            };
            let target_file_path = Path::new(target_dir).join(path);
            let target_file_path = target_file_path.to_str().unwrap();
            let mut out_file = File::create(target_file_path)?;
            let buf: &mut Vec<u8> = &mut vec![];
            file.read_to_end(buf)?;
            out_file.write_all(buf.as_slice())?;
        }
    }
    Ok(())
}


pub fn download_mi(release: &str, target_dir: &str) -> Result<(), GetResourcesError> {
    let url = format!(
        "https://github.com/google/material-design-icons/releases/download/{}/material-design-icons-{}.zip",
        release, release
    );
    download_and_unzip("material-icons", url, target_dir, Some("iconfont"))
}