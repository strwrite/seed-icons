# Icons Collections for Seed

[Seed](https://seed-rs.org/) is an awesome framework to develop
single page applications.

[Seed Icons](https://strwrite.gitlab.io/seed-icons-browser/) 
provides `build.rs` to generate icons to use with Seed.
It does not provide you with icons themselves (you stil have to reference
CSS / JS files from your index.html), but it gives you a module for each icon and it can help you with downloading resources.

## Use [Font-Awesome](https://fontawesome.com/) collection

### Add to `[dependencies]`

In your Cargo.toml:
``` toml
seed-icons = "0.4.0"
```

### Import icon

For example [regular check-circle](https://fontawesome.com/icons/check-circle?style=regular) 
could be imported next way:

``` rust
use seed_icons::fa::regular::check_circle;
```

### <a name="render_icon"></a> Render icon

There are three methods in icon module: `i`, `i_c`, `i_s` and `i_sc`

- `i` - just renders icon
- `i_c` - renders icon with a list of custom classes
- `i_s` - renders icon with style
- `i_sc` - renders icon with style and classes

For example call `i_s` function could be used like that:

``` rust
use seed::{prelude::*, *};
use seed_icons::fa::regular::check_circle;
use seed_style::*;

fn green_check<T>() -> Node<T> {
    check_circle::i_s(s().color(
        CssColor::StringValue("green".to_string())
    ))
}
```

Or `i_c` with custom CSS:

``` rust
check_circle::i_c(vec!["green-icon"])
```

In this case you'd need also to provide CSS somewhere:
``` css
.green-icon {
    color: green;
}
```

## Add resources

You might want to download resources with icons by yourself, or
you can use `seed-icons-gen` crate for that.

### Add build dependency

In `[build-dependencies]` add following:

``` toml
seed-icons-gen = "0.3.0"
```

### Add `build.rs`

If you already have `build.rs`, you just need to modify it
to do the same as the following one:

``` rust
fn main() {
    seed_icons_gen::get_fa_resources("static");
}
```

When your crate would be built, that would download `font-awesome`
folder inside of `static` folder. 

You might want to add `.gitignore` file in static folder:

``` .gitignore
font-awesome
```
, because you'd probably will not want to keep all those 
resources in your VCS.

### Link stylesheets

If you've downloaded resources as explained before, then
you can link them in `index.html` like that:

``` html
    <link href="static/font-awesome/css/all.css" rel="stylesheet">
```

## Use [material design](http://material.io/) icons

### Add to `[dependencies]`

In your Cargo.toml:
``` toml
[dependencies.seed-icons]
version = "0.4.0"
features = [
  "material_icons"
]
```

Note that material icons is comes as an additional feature, by default
only Font Awesome collection is included, which you can opt-out.

### Import icon

For example [regular check_circle](https://material.io/resources/icons/?icon=check_circle&style=baseline) 
could be imported next way:

``` rust
use seed_icons::mi::regular::check_circle;
```

### Render icon

Same as [Render icon](#render_icon) above 

### Link stylesheets

With Material Icons it's easier to link stylesheets directly
from Google Fonts like this:

``` html
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
```

If you want to bundle it in your build, you'd have to do it manually.

Alternative way to use `get_mi_resources` from `seed-icons-gen` 
in your `build.rs`, however that approach limits you to only
`regular` icons and some of the icons won't not be rendered properly.

## SEED_ICONS_FILTER_NAMES

To optimize build time, `seed-icons` build supports environment variable `SEED_ICONS_FILTER_NAMES`.

It should be semicolon-separated list of icons names, which you want 
to limit build to. While building the crate, `seed-icons` would 
check that variable and would only include those icons, which names 
are in that list. 

Names are not always match name of module in the `icons.rs`, you might
want to visit [Seed Icons Browser](https://strwrite.gitlab.io/seed-icons-browser/#/browser) 
to see the exact name of the icon you're interested in.

Here's example for `bash`:

``` bash
export SEED_ICONS_FILTER_NAMES='check_circle;check-circle;file-times;check_box_outline_blank'
```

If you add such line in `~/.bashrc`, for example, then all the build run 
from `bash` would use only limited set of icons.

Note that filter is cross-collection, therefore you cannot specify 
from which collection the icon name is. If in the features you've 
selected more than one collection and there's an icon with same name
in both of them, specifying that name in the filter variable, would
be interpreted as if you want to have both icons in the produced
`icons.rs`. Filtering up to icon from specific collection is not 
available, as this feature is only intended to be an optimization.

## Roadmap 

- add feather-icons collection https://github.com/feathericons/feather
- fix `get_mi_resources` to get all available icons, not only regular