// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

extern crate wasm_bindgen_test;
use wasm_bindgen_test::*;

wasm_bindgen_test_configure!(run_in_browser);

#[cfg(feature="styles")]
#[cfg(feature="font_awesome")]
#[wasm_bindgen_test]
fn compiles_with_green_color_style() {
    use seed_style::*;
    use seed_icons::fa::regular::check_circle;

    console_log!(
        "{:?}",
        check_circle::i_s::<()>(s().color(seed_style::CssColor::StringValue("green".to_string())))
    );
}

#[cfg(feature="styles")]
#[cfg(feature="font_awesome")]
#[wasm_bindgen_test]
fn compiles_with_green_color_style_and_custom_class() {
    use seed_style::*;
    use seed_icons::fa::regular::check_circle;
    console_log!(
        "{:?}",
        check_circle::i_sc::<()>(
            s().color(seed_style::CssColor::StringValue("green".to_string())),
            vec!["custom_class"]
        )
    );
}
