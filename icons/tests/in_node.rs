// Copyright (c) 2020 Timur Sultanaev All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

extern crate wasm_bindgen_test;
use wasm_bindgen_test::*;

#[cfg(feature = "font_awesome")]
#[wasm_bindgen_test]
fn check_circle_with_one_class() {
    use seed_icons::fa::regular::check_circle;

    // TODO: figure out a more clear way of testing the output
    assert_eq!(
        format!("{:?}", check_circle::i_c::<()>(vec!["green-icon"])),
        r#"Element(El { tag: I, attrs: Attrs { vals: {Class: Some("far fa-check-circle green-icon")} }, style: Style { vals: {} }, event_handler_manager: EventHandlerManager { groups: {} }, children: [], namespace: None, node_ws: None, refs: [], key: None })"#
    );
}
